package ru.t1.strelcov.tm.enumerated;

public enum EntityEventType {

    POST_LOAD,
    PRE_PERSIST,
    POST_PERSIST,
    PRE_UPDATE,
    POST_UPDATE,
    PRE_REMOVE,
    POST_REMOVE

}
