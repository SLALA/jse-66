package ru.t1.strelcov.tm.api;

public interface IPropertyService {

    String getMQConnectionFactory();

    String getMongoHost();

    Integer getMongoPort();

}
