package ru.t1.strelcov.tm.bootstrap;

import lombok.Getter;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.api.IReceiverService;
import ru.t1.strelcov.tm.listener.LoggerListener;

@Component
@Getter
public final class Bootstrap {

    @Autowired
    private LoggerListener loggerListener;

    @Autowired
    private IReceiverService receiverService;

    public void run(@Nullable final String[] args) {
        receiverService.receive(loggerListener);
    }

}
