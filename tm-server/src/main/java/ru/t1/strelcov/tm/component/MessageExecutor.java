package ru.t1.strelcov.tm.component;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import ru.t1.strelcov.tm.api.service.IJmsSenderService;
import ru.t1.strelcov.tm.dto.EntityEvent;
import ru.t1.strelcov.tm.enumerated.EntityEventType;
import ru.t1.strelcov.tm.service.JmsSenderService;

import javax.persistence.Table;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public final class MessageExecutor {

    private static final int THREAD_COUNT = 3;

    @NotNull
    private final IJmsSenderService service = new JmsSenderService();

    @NotNull
    private final ExecutorService es = Executors.newFixedThreadPool(THREAD_COUNT);

    public void sendMessage(@NotNull final Object object, @NotNull EntityEventType type) {
        es.submit(() -> {
            @NotNull final String json = getEntityEventJson(object, type);
            service.send(json);
        });
    }

    public void stop() {
        es.shutdown();
    }

    @SneakyThrows
    private String getEntityEventJson(@NotNull final Object entity, @NotNull EntityEventType type) {
        @NotNull final Table table = entity.getClass().getAnnotation(Table.class);
        @NotNull final String tableName = table.name();
        @NotNull final EntityEvent event = new EntityEvent(type, entity, tableName);
        @NotNull final ObjectMapper objectMapper = new ObjectMapper();
        return objectMapper.writerWithDefaultPrettyPrinter().writeValueAsString(event);
    }

}
