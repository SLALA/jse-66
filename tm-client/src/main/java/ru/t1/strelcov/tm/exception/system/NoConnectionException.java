package ru.t1.strelcov.tm.exception.system;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class NoConnectionException extends AbstractException {

    public NoConnectionException() {
        super("Error: Not connected to server.");
    }

}
