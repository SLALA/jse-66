package ru.t1.strelcov.tm.listener.system;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.request.ServerVersionRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.listener.AbstractListener;

@Component
public final class DisplayVersionServerListener extends AbstractListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "server-version";
    }

    @NotNull
    @Override
    public String description() {
        return "Display server version info.";
    }

    @Override
    @EventListener(condition = "@displayVersionServerListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[SERVER VERSION]");
        System.out.println(systemEndpoint.getVersion(new ServerVersionRequest()).getVersion());
    }

}
