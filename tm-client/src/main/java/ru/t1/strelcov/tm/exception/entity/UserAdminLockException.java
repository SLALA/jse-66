package ru.t1.strelcov.tm.exception.entity;

import ru.t1.strelcov.tm.exception.AbstractException;

public final class UserAdminLockException extends AbstractException {

    public UserAdminLockException() {
        super("Error: You cannot lock Administrator.");
    }

}
