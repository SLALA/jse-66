package ru.t1.strelcov.tm.service;

import com.jcabi.manifests.Manifests;
import lombok.Getter;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.service.IPropertyService;

@Getter
@PropertySource("classpath:application.properties")
@Service
public class PropertyService implements IPropertyService {

    @NotNull
    @Value("${server.host:localhost}")
    public String serverHost;

    @NotNull
    @Value("${server.port:8080}")
    public Integer serverPort;

    @NotNull
    @Override
    public String getName() {
        return Manifests.read("developer");
    }

    @NotNull
    @Override
    public String getVersion() {
        return Manifests.read("build");
    }

    @NotNull
    @Override
    public String getEmail() {
        return Manifests.read("email");
    }

}