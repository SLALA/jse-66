package ru.t1.strelcov.tm.service;

import org.jetbrains.annotations.NotNull;
import org.springframework.stereotype.Service;
import ru.t1.strelcov.tm.api.service.ICommandService;
import ru.t1.strelcov.tm.exception.system.UnknownCommandException;
import ru.t1.strelcov.tm.listener.AbstractListener;
import ru.t1.strelcov.tm.listener.user.UserLockByLoginListener;

import java.util.Arrays;
import java.util.Collection;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public final class CommandService implements ICommandService {

    //  @Autowired
    private AbstractListener[] commands = {new UserLockByLoginListener()};

    @NotNull
    @Override
    public Collection<AbstractListener> getCommands() {
        return Arrays.stream(commands).collect(Collectors.toList());
    }

    @NotNull
    @Override
    public AbstractListener getCommandByName(@NotNull final String name) {
        return getCommands().stream().filter(c -> name.equals(c.name())).findFirst().orElseThrow(() -> new UnknownCommandException(name));
    }

    @NotNull
    @Override
    public AbstractListener getCommandByArg(@NotNull final String arg) {
        return getArguments().stream().filter(a -> arg.equals(a.arg())).findFirst().orElseThrow(() -> new UnknownCommandException(arg));
    }

    @NotNull
    @Override
    public Collection<AbstractListener> getArguments() {
        return Arrays.stream(commands).filter(c -> Optional.ofNullable(c.arg()).filter(l -> !l.isEmpty()).isPresent()).collect(Collectors.toList());
    }

}
