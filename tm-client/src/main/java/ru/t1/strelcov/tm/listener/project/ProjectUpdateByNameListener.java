package ru.t1.strelcov.tm.listener.project;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;
import ru.t1.strelcov.tm.dto.model.ProjectDTO;
import ru.t1.strelcov.tm.dto.request.ProjectUpdateByNameRequest;
import ru.t1.strelcov.tm.event.ConsoleEvent;
import ru.t1.strelcov.tm.util.TerminalUtil;

@Component
public final class ProjectUpdateByNameListener extends AbstractProjectListener {

    @Nullable
    @Override
    public String arg() {
        return null;
    }

    @NotNull
    @Override
    public String name() {
        return "project-update-by-name";
    }

    @NotNull
    @Override
    public String description() {
        return "Update project by name.";
    }

    @Override
    @EventListener(condition = "@projectUpdateByNameListener.name() == #event.name")
    public void handler(@NotNull final ConsoleEvent event) {
        System.out.println("[UPDATE PROJECT BY NAME]");
        System.out.println("ENTER PROJECT NAME:");
        final String oldName = TerminalUtil.nextLine();
        System.out.println("ENTER NEW NAME:");
        @NotNull final String name = TerminalUtil.nextLine();
        System.out.println("ENTER NEW DESCRIPTION:");
        @NotNull final String description = TerminalUtil.nextLine();
        @NotNull final ProjectDTO project = projectEndpoint.updateByNameProject(new ProjectUpdateByNameRequest(getToken(), oldName, name, description)).getProject();
        showProject(project);
    }

}
