<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<jsp:include page="../include/_header.jsp"/>

<h1>Edit project</h1>

<form:form action="/project/edit/${project.id}/" method="post" modelAttribute="project">
    <p>
    <div>Id:</div>
    <form:input type="text" path="id" disabled="true"/>
    </p>
    <p>
    <div>Name:</div>
    <div>
        <form:input type="text" path="name"/>
    </div>
    </p>
    <p>
    <div>Description:</div>
    <div>
        <form:input type="text" path="description"/>
    </div>
    </p>
    <p>
    <div>Status:</div>
    <div>
        <form:select path="status">
            <form:option value="${null}" label="--"/>
            <form:options items="${statuses}" itemLabel="displayName"/>
        </form:select>
    </div>
    </p>
    <p>
    <div>Date Start:</div>
    <div>
        <form:input type="datetime-local" path="dateStart"/>
    </div>
    </p>
    <button type="submit">Save</button>
</form:form>

<jsp:include page="../include/_footer.jsp"/>