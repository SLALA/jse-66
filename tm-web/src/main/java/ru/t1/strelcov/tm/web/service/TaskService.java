package ru.t1.strelcov.tm.web.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.web.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.web.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.web.api.service.ITaskService;
import ru.t1.strelcov.tm.web.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.web.exception.entity.TaskNotFoundException;
import ru.t1.strelcov.tm.web.model.Task;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class TaskService implements ITaskService {

    @NotNull
    @Autowired
    ITaskRepository taskRepository;

    @NotNull
    @Autowired
    IProjectRepository projectRepository;

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAll() {
        return taskRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Task::getName))
                .collect(Collectors.toList());
    }

    @Transactional
    @SneakyThrows
    @Override
    public void add(@Nullable final Task entity) {
        if (entity == null) return;
        taskRepository.save(entity);
    }

    @Transactional
    @SneakyThrows
    @Override
    public Task add(@NotNull final String name, @Nullable final String description) {
        @NotNull Task task = new Task(name, description);
        add(task);
        return task;
    }

    @Transactional
    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<Task> list) {
        @Nullable List<Task> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        taskRepository.saveAll(listNonNull);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void clear() {
        taskRepository.deleteAll();
    }

    @SneakyThrows
    @NotNull
    @Override
    public Task findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return taskRepository.findById(id).orElseThrow(TaskNotFoundException::new);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public Task deleteById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final Task entity = taskRepository.findById(id).orElseThrow(TaskNotFoundException::new);
        taskRepository.deleteById(id);
        return entity;
    }

    @SneakyThrows
    @NotNull
    @Override
    public List<Task> findAllByProjectId(@Nullable final String projectId) {
        Optional.ofNullable(projectId).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return taskRepository.findAllByProjectId(projectId);
    }

}
