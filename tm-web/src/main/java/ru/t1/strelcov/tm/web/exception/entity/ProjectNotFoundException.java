package ru.t1.strelcov.tm.web.exception.entity;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class ProjectNotFoundException extends AbstractException {

    public ProjectNotFoundException() {
        super("Error: Project not found.");
    }

}
