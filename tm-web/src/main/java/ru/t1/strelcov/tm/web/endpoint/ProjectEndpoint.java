package ru.t1.strelcov.tm.web.endpoint;

import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import ru.t1.strelcov.tm.web.api.service.IProjectService;
import ru.t1.strelcov.tm.web.client.ProjectEndpointClient;
import ru.t1.strelcov.tm.web.model.Project;

import java.util.List;

@RestController
@RequestMapping("/api/projects")
public class ProjectEndpoint implements ProjectEndpointClient {

    @NotNull
    @Autowired
    private IProjectService projectService;

    @Override
    @GetMapping("/findAll")
    public List<Project> findAll() throws Exception {
        return projectService.findAll();
    }

    @Override
    @PostMapping("/add")
    public void add(@RequestBody @NotNull final Project project) throws Exception {
        projectService.add(project);
    }

    @Override
    @PostMapping("/addAll")
    public void addAll(@RequestBody @NotNull final List<Project> list) throws Exception {
        projectService.addAll(list);
    }

    @Override
    @PostMapping("/addByName")
    public Project addByName(@RequestParam final String name, @RequestParam final String description) {
        return projectService.add(name, description);
    }

    @Override
    @GetMapping("/findById/{id}")
    public Project findById(@PathVariable("id") @NotNull final String id) throws Exception {
        return projectService.findById(id);
    }

    @Override
    @PostMapping("/deleteById/{id}")
    public void deleteById(@PathVariable("id") @NotNull final String id) {
        projectService.deleteById(id);
    }

    @Override
    @PostMapping("/deleteAll")
    public void clear() {
        projectService.clear();
    }

}
