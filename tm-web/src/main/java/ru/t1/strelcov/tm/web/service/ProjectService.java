package ru.t1.strelcov.tm.web.service;

import lombok.SneakyThrows;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.t1.strelcov.tm.web.api.repository.IProjectRepository;
import ru.t1.strelcov.tm.web.api.repository.ITaskRepository;
import ru.t1.strelcov.tm.web.api.service.IProjectService;
import ru.t1.strelcov.tm.web.exception.empty.EmptyIdException;
import ru.t1.strelcov.tm.web.exception.entity.ProjectNotFoundException;
import ru.t1.strelcov.tm.web.model.Project;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

@Service
public class ProjectService implements IProjectService {

    @NotNull
    @Autowired
    IProjectRepository projectRepository;

    @NotNull
    @Autowired
    ITaskRepository taskRepository;

    @SneakyThrows
    @NotNull
    @Override
    public List<Project> findAll() {
        return projectRepository.findAll()
                .stream()
                .sorted(Comparator.comparing(Project::getName))
                .collect(Collectors.toList());
    }

    @Transactional
    @SneakyThrows
    @Override
    public void add(@Nullable final Project entity) {
        if (entity == null) return;
        projectRepository.save(entity);
    }

    @Transactional
    @SneakyThrows
    @Override
    public Project add(@NotNull final String name, @Nullable final String description) {
        @NotNull Project project = new Project(name, description);
        add(project);
        return project;
    }

    @Transactional
    @SneakyThrows
    @Override
    public void addAll(@Nullable final List<Project> list) {
        @Nullable List<Project> listNonNull = Optional.ofNullable(list).map(l -> l.stream().filter(Objects::nonNull).collect(Collectors.toList())).orElse(null);
        if (listNonNull == null || listNonNull.size() == 0) return;
        projectRepository.saveAll(listNonNull);
    }

    @Transactional
    @SneakyThrows
    @Override
    public void clear() {
        projectRepository.deleteAll();
    }

    @SneakyThrows
    @NotNull
    @Override
    public Project findById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        return projectRepository.findById(id).orElseThrow(ProjectNotFoundException::new);
    }

    @Transactional
    @SneakyThrows
    @NotNull
    @Override
    public Project deleteById(@Nullable final String id) {
        Optional.ofNullable(id).filter((i) -> !i.isEmpty()).orElseThrow(EmptyIdException::new);
        final Project entity = projectRepository.findById(id).orElseThrow(ProjectNotFoundException::new);
        projectRepository.deleteById(id);
        taskRepository.deleteAllByProjectId(id);
        return entity;
    }

}
