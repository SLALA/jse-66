package ru.t1.strelcov.tm.web.exception.entity;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class TaskNotFoundException extends AbstractException {

    public TaskNotFoundException() {
        super("Error: Task not found.");
    }

}
