package ru.t1.strelcov.tm.web.exception.entity;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class EmptyProjectIdException extends AbstractException {

    public EmptyProjectIdException() {
        super("Error: Project id is empty.");
    }

}
