package ru.t1.strelcov.tm.web.exception.empty;

import ru.t1.strelcov.tm.web.exception.AbstractException;

public final class EmptyIdException extends AbstractException {

    public EmptyIdException() {
        super("Error: Id is empty.");
    }

}
