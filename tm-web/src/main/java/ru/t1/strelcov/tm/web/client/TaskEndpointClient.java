package ru.t1.strelcov.tm.web.client;

import feign.Feign;
import org.jetbrains.annotations.NotNull;
import org.springframework.beans.factory.ObjectFactory;
import org.springframework.boot.autoconfigure.web.HttpMessageConverters;
import org.springframework.cloud.netflix.feign.support.SpringDecoder;
import org.springframework.cloud.netflix.feign.support.SpringEncoder;
import org.springframework.cloud.netflix.feign.support.SpringMvcContract;
import org.springframework.http.converter.FormHttpMessageConverter;
import org.springframework.web.bind.annotation.*;
import ru.t1.strelcov.tm.web.model.Task;

import java.util.List;

public interface TaskEndpointClient {

    @NotNull
    String BASE_URL = "http://localhost:8080/api/tasks";

    static TaskEndpointClient client() {
        @NotNull final FormHttpMessageConverter converter = new FormHttpMessageConverter();
        @NotNull final HttpMessageConverters converters = new HttpMessageConverters(converter);
        @NotNull final ObjectFactory<HttpMessageConverters> objectFactory = () -> converters;
        return Feign.builder()
                .contract(new SpringMvcContract())
                .encoder(new SpringEncoder(objectFactory))
                .decoder(new SpringDecoder(objectFactory))
                .target(TaskEndpointClient.class, BASE_URL);
    }

    @GetMapping("/findAll")
    List<Task> findAll() throws Exception;

    @PostMapping("/add")
    void add(@RequestBody Task task) throws Exception;

    @PostMapping("/addAll")
    void addAll(@RequestBody List<Task> list) throws Exception;

    @PostMapping("/addByName")
    Task addByName(@RequestParam("name") final String name, @RequestParam("description") final String description) throws Exception;

    @GetMapping("/findById/{id}")
    Task findById(@PathVariable("id") String id) throws Exception;

    @GetMapping("/findAllByProjectId/{projectId}")
    List<Task> findAllByProjectId(@PathVariable("projectId") String projectId) throws Exception;

    @PostMapping("/deleteById/{id}")
    void deleteById(@PathVariable("id") String id) throws Exception;

    @PostMapping("/deleteAll")
    void clear() throws Exception;

}
